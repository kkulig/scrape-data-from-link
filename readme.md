# Scrape data 

Scrape data from this link:

https://www.auchandirect.pl/auchan-warszawa/pl/search?text=pepsi+cola&callback=true

Data: title, price, image, packaging.

# Important 
* Run all cell
* Download data will be in ./data/file.csv
* Pictures will be in ./data/

# Example Results
![](example.jpg)

# Stack
* Python 3
* selenium, webdriver
* time
* os
* pandas
* urllib.request
* Jupyter Notebook